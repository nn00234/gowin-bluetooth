import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './components/button/button.component';
import { LabelComponent } from './components/label/label.component';
import { LoaderComponent } from './components/loader/loader.component';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { SvgImgLoaderComponent } from './components/svg-img-loader/svg-img-loader.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    LabelComponent,
    LoaderComponent,
    SplashScreenComponent,
    SvgImgLoaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
