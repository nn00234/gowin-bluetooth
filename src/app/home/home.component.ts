import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public greetings: string = '';
  public selectedCard: string = '';
  public active = false;
  public state = 'active';
  public buttonTitle = 'Start';

  constructor(private router: Router) { }

  ngOnInit() {
    this.getGreetings();
  }

  private getGreetings(): void {
    const date = new Date();
    const currentTime = Number(moment(date).format('HH'));
    this.greetings = `Good ${this.greetingMsg(currentTime)}`;
  }

  private greetingMsg(time: number) {
    if (time === 0 || time < 12) {
      return 'morning';
    }
    if (time === 12 || time < 16) {
      return 'afternoon';
    }
    return 'evening';
  }

  public setActive(state: boolean): void {
    this.active = state;
  }

  public tapEvent(payload: string) {
    console.log('TAP-EVENT', payload);
    // this.router.navigate(['dashboard']);
  }
}
