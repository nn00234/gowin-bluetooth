import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})

export class CommonService {
  private appStore: Map<string, any> = new Map<string, any>();
  constructor() {}

  public setValue<T>(key: string, value: any): void {
    this.appStore.set(key, value);
  }

  public getValue<T>(key: string): T {
    return this.appStore.get(key);
  }

  public removeItem(key: string): void {
    this.appStore.delete(key);
  }

  public clearStore() {
    this.appStore.clear();
  }

}
