import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { SVGState } from './svg-loader.variation';

@Component({
  selector: 'app-svg-img-loader',
  templateUrl: './svg-img-loader.component.html',
  styleUrls: ['./svg-img-loader.component.scss']
})
export class SvgImgLoaderComponent implements OnInit {
  @Input() src: string = '';
  @Input() adjustViewBox: boolean = false;
  @Input() state: SVGState = 'normal';

  public heightSize: number = 100;
  public widthSize: number = 100;
  public backgroundSize: number = 100;
  public svgContent: SafeHtml = '';
  public updateShimmer: string = '';

  constructor(
    private sanitized: DomSanitizer
  ) { }

  ngOnInit() {
  }

}
