export type SVGLoaderVariation = 'xsmal' |
  'small' |
  'smaller' |
  'xmedium' |
  'medium' |
  'medium-large';

export type SVGState = 'shimmer' | 'normal' | 'shimmer-light' | 'shimmer-dark';

export const svgLoaderVariationType: SVGLoaderVariation[] = ['xsmal', 'small', 'smaller', 'xmedium', 'medium', 'medium-large'];

