import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { ResponseContentType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class SvgLoaderService {
  public cachedObservables: any = {};

  constructor(
    private httpClient: HttpClient,
    private ngZone: NgZone
  ) { }

  public getData(url: string): Observable<string> {
    return this.ngZone.runOutsideAngular(() => {
      if (!this.cachedObservables[url]) {
        const obs = new ReplaySubject<string>(1);
        // const headers = new HttpHeaders();
        this.cachedObservables[url] = obs.asObservable();
        this.httpClient.get<string>(url).pipe(
          timeout(5000)
        ).subscribe({
          next: content => {
            obs.next(content);
          },
          error: err => {
            console.error(`Load Error: ${url}`, err);
            obs.error(err);
            delete this.cachedObservables[url];
          },
          complete: () => obs.complete()
        });
      }
      return this.cachedObservables[url];
    })
  }
}
