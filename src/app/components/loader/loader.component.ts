import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  @Input() showLoader: boolean = true;
  public windowHeight: string = '';

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.windowHeight = `-${window.innerHeight}px`
    }, 3000);
  }

}
