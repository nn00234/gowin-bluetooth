import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';



@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
@Input() state: string = 'active';
@Input() btnContent: string = '';
@Input() active: boolean = true;
@Input() payload: any = '';
@Output() btnTouchEvent = new EventEmitter<any>()

  constructor() { }

  ngOnInit() {
  }

  public setActive(state: boolean): void {
    this.active = state;
  }

  public tapEvent(): void {
    console.log('tapEvent', this.payload);
    this.btnTouchEvent.emit(this.payload);
  }

}
